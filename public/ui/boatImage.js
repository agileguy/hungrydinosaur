var BoatImage = function (image, messageLayer, player, thisPlayerName, opponentImage) {
// boat
    var boatImg = new Kinetic.Shape(function () {
        var context = this.getContext();
        var width = 50, height = 50, x = 500 + (50 * player.x), y = 300 + (50 * player.y);
        context.translate(x, y);
        context.rotate(player.direction);
        var imageToDraw = player.nickname === thisPlayerName ? image : opponentImage;
        context.drawImage(imageToDraw, -width / 2, -height / 2, width, height);
        context.rotate(-player.direction);
        context.translate(-x, -y);
        context.font = "12pt Calibri";
        context.fontStyle = "bold";
        context.fillStyle = player.nickname === thisPlayerName ? "yellow" : "red";
        context.fillText((player.nickname || ''), x, y);
    });

    return boatImg;
};