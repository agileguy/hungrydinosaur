var stage, messageLayer, imageLayer, toolLayer, seaImg, boatImg;

function loadImages(sources, callback, players, ownName, food){
    var images = {};
    var loadedImages = 0;
    var numImages = 0;
    for (var src in sources) {
        numImages++;
    }
    for (var src in sources) {
        images[src] = new Image();
        images[src].onload = function(){
            if (++loadedImages >= numImages) {
                callback(images, players, ownName, food);
            }
        };
        images[src].src = sources[src];
    }
}

function writeMessage(messageLayer, message){
    var context = messageLayer.getContext();
    messageLayer.clear();
    context.font = "12pt Calibri";
    context.fontStyle = "bold";
    context.fillStyle = "white";
    context.fillText(message, 10, 280);
}


function drawMap(images, players, ownName, food){
    var initialised = (stage === null);
    stage = stage || new Kinetic.Stage("container", 1000, 600);

    imageLayer = imageLayer || new Kinetic.Layer();
    messageLayer = messageLayer || new Kinetic.Layer();

    imageLayer.clear();
    // sea
    if (seaImg) { imageLayer.remove(seaImg);}
    seaImg = seaImg || new Kinetic.Shape(function(){
        var context = this.getContext();
        context.drawImage(images.sea, 0, 0, 1000, 600);

    });

    imageLayer.add(seaImg);

    // players
    for(var player in players){
        imageLayer.add(new BoatImage(images.boat, messageLayer, players[player], ownName, images.opponent));
    }

    // food
    if (food) { imageLayer.add(new Food(images.ham, food)); }

    // layers
    if (!initialised)
    {


        stage.add(imageLayer);
        stage.add(messageLayer);
    }

}

function move(command) {
                    socket.emit('user message', command);
                }

function draw (players, ownName, food) {
    var sources = {
        boat: "images/gooddino.png",
        sea: "images/earth.jpg",
        opponent: "images/baddino.png",
        ham: "images/ham.png",
        wheel: "images/wheel.png"
    };
    loadImages(sources, drawMap, players, ownName, food);
}

