var Toolbar = function(toolLayer, images){
    var wheelImg = new Kinetic.Shape(function(){
            var context = this.getContext();
            context.drawImage(images.wheel, 0, 0, 75, 75);

        });

    var glassImg = new Kinetic.Shape(function(){
            var context = this.getContext();
            context.drawImage(images.glass, 100, 100, 75, 75);

        });

    var chestImg = new Kinetic.Shape(function(){
            var context = this.getContext();
            context.drawImage(images.chest, 100, 200, 75, 75);

        });

    toolLayer.add(wheelImg);
    toolLayer.add(chestImg);
    toolLayer.add(glassImg);
};