
var express = require('express')
  , stylus = require('stylus')
  , nib = require('nib')
  , sio = require('socket.io');

var app = express.createServer();


app.configure(function () {
  app.use(stylus.middleware({ src: __dirname + '/public', compile: compile }));
  app.use(express.static(__dirname + '/public'));
  app.set('views', __dirname);
  app.set('view engine', 'jade');

  function compile (str, path) {
    return stylus(str)
      .set('filename', path)
      .use(nib());
  }
});

app.get('/', function (req, res) {
  res.render('index', { layout: false });
});

app.listen(5000, function () {
  var addr = app.address();
  console.log('   app listening on http://' + addr.address + ':' + addr.port);
});

var io = sio.listen(app)
  , nicknames = {}
  , players = {}
  , food = { x: 3, y: -2};


io.sockets.on('connection', function (socket) {

    socket.on('user message', function (msg) {
        if (msg === '::s')
        {
            socket.player.y ++;
            socket.player.direction = Math.PI;
            validatePosition(socket.player);
            checkFood(socket.player);
            delete nicknames[socket.nickname];
            nicknames[socket.nickname] = { nickname:socket.nickname, player: socket.player};
        }
        if (msg === '::sw')
        {
            socket.player.y ++;
            socket.player.x --;
            validatePosition(socket.player);
            checkFood(socket.player);
            socket.player.direction = 2 * Math.PI * 225/ 360;
            delete nicknames[socket.nickname];
            nicknames[socket.nickname] = { nickname:socket.nickname, player: socket.player};
        }
        if (msg === '::se')
        {
            socket.player.y ++;
            socket.player.x ++;
            validatePosition(socket.player);
            checkFood(socket.player);
            socket.player.direction = 2 * Math.PI * 135 /360;
            delete nicknames[socket.nickname];
            nicknames[socket.nickname] = { nickname:socket.nickname, player: socket.player};
        }
        if (msg === '::n')
        {
            socket.player.y --;
            socket.player.direction = 0;
            validatePosition(socket.player);
            checkFood(socket.player);
            delete nicknames[socket.nickname];
            nicknames[socket.nickname] = { nickname:socket.nickname, player: socket.player};
        }
        if (msg === '::nw')
        {
            socket.player.y --;
            socket.player.x --;
            validatePosition(socket.player);
            checkFood(socket.player);
            socket.player.direction = 2 * Math.PI * 315/ 360;
            delete nicknames[socket.nickname];
            nicknames[socket.nickname] = { nickname:socket.nickname, player: socket.player};
        }
        if (msg === '::ne')
        {
            socket.player.y --;
            socket.player.x ++;
            validatePosition(socket.player);
            checkFood(socket.player);
            socket.player.direction = 2 * Math.PI * 45 /360;
            delete nicknames[socket.nickname];
            nicknames[socket.nickname] = { nickname:socket.nickname, player: socket.player};
        }
        if (msg === '::e')
        {
            socket.player.x ++;
            socket.player.direction = 2 * Math.PI * 90 /360;
            validatePosition(socket.player);
            checkFood(socket.player);
            delete nicknames[socket.nickname];
            nicknames[socket.nickname] = { nickname:socket.nickname, player: socket.player};
        }
        if (msg === '::w')
        {
            socket.player.x --;
            socket.player.direction = 2 * Math.PI * 270 /360;
            validatePosition(socket.player);
            checkFood(socket.player);
            delete nicknames[socket.nickname];
            nicknames[socket.nickname] = { nickname:socket.nickname, player: socket.player};
        }
    //socket.broadcast.emit('user message', socket.nickname, msg);
    io.sockets.emit('world', players, food);

  });
  function validatePosition(player){
      player.y = player.y > 6 ? 6 : player.y;
      player.y = player.y < -6 ? -6 : player.y;
      player.x = player.x > 10 ? 10 : player.x;
      player.x = player.x < -10 ? -10 : player.x;
      return player;
  }

  function checkFood(player)
  {
      if (player.x === food.x && player.y === food.y)
      {
          player.score++;
          generateFood();
      }
  }
  function generateFood()
  {
      food.x = 10 - Math.floor(Math.random()*21);
      food.y = 6 - Math.floor(Math.random()*11);
  }
  socket.on('nickname', function (nick, fn) {
    if (nicknames[nick]) {
      fn(true);

    } else {
      socket.nickname = nick;
      socket.player = {x:0, y:0, direction: Math.PI /4, nickname:socket.nickname, score: 0 };

      nicknames[nick] =  { nickname:nick };
      players[nick] = socket.player;
      socket.broadcast.emit('announcement', nick + ' connected');
      io.sockets.emit('nicknames', nicknames, players, food);

      fn(false);

    }
  });

  socket.on('disconnect', function () {
    if (!socket.nickname) return;

    delete nicknames[socket.nickname];
    delete players[socket.nickname];

    socket.broadcast.emit('announcement', socket.nickname + ' disconnected');
    socket.broadcast.emit('nicknames', nicknames);
  });


});
